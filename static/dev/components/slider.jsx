import React, { PureComponent } from 'react'
import '../styles/slider'

class Slider extends PureComponent {
    constructor(props) {
        super(props)

        // datos que se carga (peticion devuelta json del servidor)
        this.imagenes = [
            {
                src: './static/img/baner/img1.png',
            },
            {
                src: './static/img/baner/img2.png',
            },
            {
                src: './static/img/baner/img3.png',
            },
            {
                src: './static/img/baner/img4.png',
            },
        ]

        this.state = {
            position: 0,
            slider: 0,
        }
         
    }

    next = () => {
        const max = (this.imagenes.length * 100) - 100

        this.setState({
            // slider: (this.state.slider > 0) ? (this.state.slider + 100) : 0
            slider: (this.state.slider >= max) ? max : (this.state.slider + 100)
        })

    }

    previous = () => {
        this.setState({
            slider: (this.state.slider == 0) ? 0 : (this.state.slider - 100)
        })
    }

    render() {
        return (
            <div className='slider-menu'>

                <div className="arrows">
                    <img src="./static/img/arrow/left.svg" onClick={this.previous} />
                    <img src="./static/img/arrow/right.svg" onClick={this.next} />
                </div>
                <figure style={{
                    left: `-${this.state.slider}%`
                }}>
                    {
                        this.imagenes.map(
                            (imagen, index) => {
                                return <img src={imagen.src} alt="slider" key={index} draggable={false} />
                            }
                        )
                    }
                </figure>
            </div>
        ) 
    }
}

export default Slider