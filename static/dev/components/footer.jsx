import React, { PureComponent } from 'react'
import Menu from './menu'
import '../styles/footer'

// creacion de pie de pagina
class Footer extends PureComponent {
    render() {
        return (
            <footer>
                <div className="row content">
                    <div className="title">
                        <b>RP SIRZA CONCEPTOS LLC</b>
                        <a href="https://www.facebook.com/ConceptosRPSirza" className="facebook" target='_blank'></a>
                        <a href="https://www.instagram.com/sirza_conceptos/" className="instagram" target='_blank'></a>
                    </div>
                    <p>
                        Nunca he sido pobre, solo he estado sin dinero. Ser pobre es un Estado Mental 
                        y no tener dinero es una condición temporal.
                    </p>
                    <p>
                        <b>Oscar Javier Martínez, <b>RP SIRZA CONCEPTOS LLC.</b></b> 
                    </p>
                    <p>
                        <b>RP SIRZA CONCEPTOS LLC</b> es una franquicia autorizada por <b>ROYAL PRESTIGE</b>
                    </p>

                    <Menu footer={true} slider={false} />
                </div>
                <div className="row content-logo">
                    <img src="./static/img/logo.png" alt="logo" />
                </div>
            </footer>
        )
    }
}

export default  Footer