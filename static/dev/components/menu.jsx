import React, { PureComponent } from 'react'
import '../styles/menu'
import { Link } from 'react-router-dom'
import MediaQuery from 'react-responsive'
import Slider from './slider'

class Menu extends PureComponent {
    constructor(props) {
        super(props)

        this.state = {
            active: false
        }

        // Elementos quemados para mostrar
        this.elementos = [
            {
                texto: 'inicio',
                ruta: '/',
                especial: false,
            },
            {
                texto: 'comunidad',
                ruta: '/comunidad',
                especial: false,
            },
            {
                texto: 'historia',
                ruta: '/historia',
                especial: false,
            },
            // {
            //     texto: 'blog',
            //     ruta: '/blog',
            //     especial: false,
            // },
            {
                texto: 'contacto',
                ruta: '/contacto',
                especial: false,
            },
            {
                texto: 'aplicar',
                ruta: '/aplicar',
                especial: true,
            }
        ]
    }

    item = (props, index) => {
        return (
            <Link to={props.ruta} key={index} className={(window.location.hash == `#${props.ruta}`) ? 'active' : null + (props.especial) ? 'especial' : null}>
                {props.texto}
            </Link>
        )
    }

    itemPhone = (props, index) => {
        return (
            <li className={(window.location.hash == `#${props.ruta}`) ? 'active' : null + (props.especial) ? 'especial' : null} key={index}>
                <Link to={props.ruta}>
                    {props.texto}
                </Link>
            </li>
        )
    }

    render() {
        return (
            <div className="content-menu">
                {/* Render de menu solo SI está en un dispositivo laptop o ordenador */}
                <MediaQuery minDeviceWidth={1224}>
                    <nav className={(this.props.footer) ? 'menu-footer' : 'menu-main'}>
                        {
                            this.elementos.map(this.item)
                        }
                    </nav>
                </MediaQuery>
                
                {/* Render de menu solo SI está en un dispositivo móvil */}
                <MediaQuery maxDeviceWidth={1224}>
                    <nav className={`menu-mobile ${(this.state.active) ? 'active' : null}`}>
                        <div className={`icon-menu ${(this.state.active) ? 'active' : null}`} onClick={
                            (e) => {
                                this.setState({
                                    active: !this.state.active
                                })
                            }
                        }>
                            <div className='item first'></div>
                            <div className='item last'></div>
                        </div>

                        <div className="content-logo">
                            <img src="./static/img/logo-white.svg" alt="logo" />
                        </div>

                        <ul>
                            {
                                this.elementos.map(this.itemPhone)
                            }
                        </ul>
                    </nav>
                </MediaQuery>

                {
                    // si la propiedad slider es igual a verdadera y la propiedad footer es verdadera renderizar
                    (this.props.slider && !this.props.footer) &&
                    <Slider />
                }

            </div>
        )
    }
}

export default Menu