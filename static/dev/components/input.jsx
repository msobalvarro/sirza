import React, { PureComponent } from 'react'
import '../styles/input'

class Input extends PureComponent {
    render() {
        return (
            <input 
            {...this.props}
            ref={this.props.referencia}
            className={`input-text ${this.props.className}`} 
            onChange={ (e) => { this.props.onChange(e.target.value) } } 
            required={this.props.required}/>
        )
    }
}

export default Input