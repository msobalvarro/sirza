import React, { PureComponent } from 'react'
import '../styles/loader.css'

class Loader extends PureComponent {
    constructor(props) {
        super(props)
    }

    render() {
        if (this.props.active) {
            return (
                <div className="loader-content">
                    <div class="loader1"></div>
                </div>
            )
        } else {
            return null
        }
    }
}

export default Loader