import React, { PureComponent } from 'react'
import '../styles/logo'

class Logo extends PureComponent {
    render () {
        return (
            <div className="container-logo" ref={ e => this.logo = e }>
                <div className="content"></div>
            </div>
        )
    }
}

export default Logo