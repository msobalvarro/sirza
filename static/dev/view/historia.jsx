import React, { PureComponent } from 'react'
import { Link } from 'react-router-dom'
import Menu from '../components/menu'
import Footer from '../components/footer'
import '../styles/historia'

class Historia extends PureComponent {
    render() {
        return (
            <div className="container historia">
                <Menu />

                <div className="content-historia">

                    <h2>Historia de Sirza</h2>
                    <img src="./static/img/economy.svg" />

                    <div className="contenido-historia">
                        <div className="bloque">
                            <div className="info">
                                <h3>RP SIRZA CONCEPTOS LLC:</h3>
                                <p>
                                    Se ha destacado como una de las Compañías exitosas en ventas directas, creando y formando negocios a traves de franquicias.                                   
                                </p>

                                <p>
                                    <b>RP SIRZA CONCEPTOS LLC</b> es una fuente de empleo para cualquier tipo de persona sin discriminar Raza, Género, Edad, 
                                    Nacionalidad y Libertad de Expresión.
                                </p>

                                <p>
                                    Sus Valores que lo caracterizan son: disciplina, constancia, organización, responsabilidad, compromiso, 
                                    ética, servicio y trabajo en equipo.
                                </p>

                                <p>
                                    <b>“RP SIRZA CONCEPTOS LLC”</b>; En la actualidad promedia una producción de dos millones de dólares anual y 
                                    se perfila para seguir creciendo en los próximos años en los Estados Unidos; <b>‘’Bendecido para Bendecir’’</b> Es uno de sus lemas.
                                </p>

                                <p>
                                    <b>RP SIRZA CONCEPTOS LLC</b> ha sido el vehículo para que los hispanos logren el tan anhelado sueño americano y por ende lograr bendecir familias.
                                </p>
                                
                                <p>
                                    En la actualidad nos hemos puesto el reto de compartir y formar empresarios con carácter de líder sin distinción de raza ni credo,
                                    sirviendo de puente para que miles de soñadores pueden alcanzar el éxito.
                                </p>  
                                <p>
                                    La franquicia <b>RP SIRZA CONCEPTOS LLC</b>, se siente orgulloso de trabajar para <b>ROYAL PRESTIGE</b>, compañía la cual permite que tus sueños
                                    se hagan realidad; <Link to='/contacto'>Contáctenos</Link>  y descubrenos.
                                </p>
                            </div>

                            <div className="info">
                                <h3>Acerca de Royal Prestige:</h3>
                                <p>
                                    Fundada en 1959 en Madison, Wisconsin, Estados Unidos, Royal Prestige® se ha convertido en una de las compañías 
                                    de ventas directas más exitosas en el mundo. Además de México, actualmente estamos presentes en Estados Unidos, 
                                    Canadá, Ecuador, República Dominicana, Argentina, Brasil, Colombia y Perú, entre otros.
                                </p>

                                <p>
                                    Ofrecemos los más innovadores utensilios de cocina y accesorios para el hogar, los cuales son diseñados con 
                                    exigentes estándares de calidad. Nuestro exclusivo programa de mercadeo, te permite adquirir la amplia 
                                    gama de productos en la privacidad de tu casa. Uno de nuestros Distribuidores Autorizados estará gustoso de presentártelos, 
                                    de manera honesta y personalizada. Todas las preguntas te serán cuidadosamente respondidas, atendiendo a tus necesidades específicas.
                                </p>

                                <p>
                                    Royal Prestige® y su red de Distribuidores Autorizados extiende una cordial invitación para conocer los enormes beneficios 
                                    que nuestros productos tienen para ti y tu familia.
                                </p>

                                <p>
                                    Nos sentimos orgullosos de ser miembros de la Asociación de Venta Directa, lo cual nos compromete a 
                                    seguir estándares y conductas éticas al comercializar nuestros productos y servicios.
                                </p>
                            </div>
                        </div>
                        <div className="bloque">
                            <div className="info">
                                <h3>Sirza Altamirano (Managua, Nicaragua):</h3>
                                <p>
                                    Mis padres Eli Altamirano (QEPD) & Miriam Cornejo; estudios básicos realizados en el Instituto Nacional 
                                    Maestro Gabriel (1988), egresada y graduada en la Universidad Centroamericana (UCA) en la carrera de 
                                    Ciencias Jurídica y Humanitarias (1993) incorporada como Abogada y Notario Publica ante la Corte Suprema 
                                    de Justicia de Nicaragua (1994). 
                                </p>

                                <p>
                                    Llegué como migrante legal a Estados Unidos en 2004, trabajé en labor común ganando salarios desde el mínimo $8.25 hasta $16.00; 
                                    buscando el famoso Sueño Americano en el año 2008 conocí la Oportunidad de Trabajar para la Compañía Royal Prestige, 
                                    sin tener ninguna experiencia en el campo de Marketing afronté con gran expectativa el deseo de tener en mis manos un negocio 
                                    propio, el cual alcancé en corto tiempo siguiendo paso a paso todas enseñanzas de mis mentores y patrocinadoras; en 2009 la 
                                    Compañía Royal Prestige me extendió la Franquicia RP SIRZA CONCEPTOS LLC. Desde entonces trabajando con gran dedicación y 
                                    constancia el Programa de Mercadeo que nos brinda el corporativo Hy Cite Enterprises, LLC.

                                    con todos sus beneficios que lo hacen inigualable por brindar: producto, garantía, financiamiento, servicios, facturación, publicidad, reconocimiento, educación, entrenamiento, incentivo y lo mejor crecimiento financiero.
                                </p>
                                
                            </div>

                            <div className="info">
                                <h3>Oscar Javier Martínez (Bucaramanga -Colombia):</h3>
                                <p>
                                    Nacido en la ciudad de Bucaramanga de origen colombiano, especializado como técnico mecánico industrial; 
                                    estudios en gestión empresarial, profesional en ventas y servicio al cliente.
                                </p>

                                <p>
                                    Con una experiencia en ventas por más de 20 años, donde le ha permitido desarrollar habilidades como: 
                                    Capacidad de escucha, comunicación eficaz, empatía y confianza, proactividad, paciencia e insistencia, 
                                    trabajo en equipo, planificación y organización de trabajo, autonomía y liderazgo.
                                </p>

                                <p>
                                    Oscar Javier, hoy en día ejerce como gerente de la compañía <b>RP SIRZA CONCEPTOS LLC</b>; 
                                    sus valores que lo caracterizan son: respeto, empatía, pasión, honestidad, responsabilidad, integridad, servicio y disciplina.
                                </p>

                                <p>
                                    Entre sus mayores logros obtenidos actualmente resalta:
                                </p>

                                <ul>
                                    <li>Poder llegar al país de las oportunidades.</li>
                                    <li>Poder llegar al país donde los sueños se hacen realidad.</li>
                                    <li>Poder obtener la tan anhelada libertad financiera.</li>
                                    <li>Poder obtener una franquicia propia y trabajar por su cuenta.</li>
                                    <li>Poder compartir sus sueños con otras personas sin ningún tipo de limitación.</li>
                                </ul>

                                <p>
                                    Finaliza Oscar Javier, invitando a todas las personas a seguir soñando y seguir pensando en grande; 
                                    Recordando a su vez, nunca he sido pobre, solo he estado sin dinero, ser pobre es un estado mental 
                                    y no tener dinero es una condición temporal.
                                </p>
                                                              
                            </div>
                        </div>

                    </div>


                    


                </div>

                <Footer />

            </div>
        )
    }
}

export default Historia 