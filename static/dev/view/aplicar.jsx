import React, { PureComponent } from 'react'
import Menu from '../components/menu'
import Footer from '../components/footer'
import Axios from 'axios'
import '../styles/aplicar'
import util from '../util.js'
import Loader from '../components/loader'
import swal from 'sweetalert'

class Aplicar extends PureComponent {
    constructor(props) {
        super(props)

        this.validate = this.validate.bind(this)

        this.state = {
            nombre: '',
            profesion: '',
            nacionalidad: 'default',
            correo: '',
            cv: null,
            paises: [],
            validateEmail: false,
            licencia: 'default',
            pdf: null,
            visa: null,
            loader: false,
        }

        console.log(this.state)
    }

    componentDidMount() {
        this.getPaises()
    }

    getPaises = () => {
        try {
            if (this.state.paises.length == 0) {
                const self = this

                Axios.get('https://restcountries.eu/rest/v2/region/Americas').then(
                    (response) => {
                        self.setState({ paises: response.data })
                    }
                )
            }
        } catch (error) {
            console.log(error)
        }
    }

    async validate(e) {
        e.preventDefault()
        const self = this
        let pass = true

        try {
            if (this.state.nombre.length < 5) {
                throw 'Ingrese al menos un nombre'
            }

            if (this.state.nacionalidad == 'default') {
                throw 'Seleccione una nacionalidad'
            }

            if (this.state.profesion.length == 0) {
                throw 'Ingrese una profesión u oficio'
            }

            if (this.state.licencia == 'default') {
                throw 'Seleccione una opción de licencia'
            }            

            if(!util.validateEmail(this.state.email)) {
                throw 'Ingrese una dirección de correo válido'
            }

            if(this.state.pdf == undefined || this.state.pdf.length == 0) {
                throw 'seleccione una hoja de vida'
            }
        } catch (e) {
            swal({
                icon: 'warning',
                title: 'Rellene todos los campos',
                text: e
            })

            pass = false
        } finally {
            if(pass) {
                this.setState({
                    loader: true
                })

                let form = new FormData()
                form.append('tipo', 'apply')
                form.append('nombre', this.state.nombre)
                form.append('nacionalidad', this.state.nacionalidad)
                form.append('profesion', this.state.profesion)
                form.append('licencia', this.state.licencia)
                form.append('email', this.state.correo)
                form.append('cv', this.state.pdf[0])

                console.log(form)

                await Axios.post('sendMail/index.php', form, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }).then(
                    (e) => {
                        console.log(e.data)

                        if(e.data.success) {
                            swal({
                                icon: 'success',
                                title: 'Gracias por aplicar',
                                text: 'Estaremos respondiendo a su correo lo mas antes posible'
                            })

                            // set
                        }
                    }
                )
            }

            this.setState({
                loader: false
            })
        }
    }

    render() {
        return (
            <div className="container aplicar">
                <Menu />
                <div className="content-aplicar">
                    <h2 className="title">
                        Aplica ahora
                    </h2>

                    <div className="col">
                        <div className="row line">
                            <div className="table">
                                <div className="time-line">
                                    <span className={`item ${(this.state.nombre.length > 5) ? 'success' : null}`}>1</span>
                                    <span className={`item ${(this.state.nacionalidad != 'default') ? 'success' : null}`}>2</span>
                                    <span className={`item ${(this.state.profesion.length > 0) ? 'success' : null}`}>3</span>
                                    <span className={`item ${(this.state.licencia != 'default') ? 'success' : null}`}>4</span>
                                    <span className={`item ${(this.state.validateEmail) ? 'success' : null}`}>5</span>
                                    <span className={`item ${(this.state.pdf) ? 'success' : null}`}>6</span>
                                </div>
                                <form 
                                    action='#' 
                                    className="sub-content" 
                                    ref={e => this.formulario = e}
                                    onSubmit={this.validate}>
                                    <div className="sub-row">
                                        <span>Ingresa tu Nombre y apellido</span>
                                        <input
                                            className='input-text'
                                            placeholder='nombre'
                                            onChange={nombre => this.setState({ nombre: nombre.target.value })}
                                            ref={e => this.nombre = e }
                                            required={true} />
                                    </div>

                                    <div className="sub-row">
                                        <span>Nacionalidad</span>
                                        <select
                                            defaultValue='default'
                                            onFocus={this.getPaises}
                                            required={true}
                                            onChange={
                                                (e) => {
                                                    this.setState({ nacionalidad: e.target.value })
                                                }
                                            }>
                                            <option value="default" disabled={true}>-- Selecciona una opción --</option>
                                            {
                                                (this.state.paises.length)
                                                    ?
                                                    this.state.paises.map(
                                                        (pais, index) => {
                                                            return (<option value={pais.name} key={index}>{pais.name}</option>)
                                                        }
                                                    )
                                                    : (
                                                        <option value="" disabled={true}>
                                                            espera un momento..
                                                    </option>
                                                    )
                                            }
                                        </select>
                                    </div>

                                    <div className="sub-row">
                                        <span>Ingresa tu profesión u oficio</span>
                                        <input
                                            className='input-text'
                                            placeholder='Profesión u oficio'
                                            ref={e => this.profesion = e}
                                            onChange={profesion => this.setState({ profesion: profesion.target.value })}
                                            required={true} />
                                    </div>

                                    <div className="sub-row">
                                        <span>Licencia de Condicir</span>
                                        <select defaultValue='default' 
                                            onChange={
                                                (e) => {
                                                    this.setState({
                                                        licencia: e.target.value
                                                    })
                                                }
                                            }>
                                            <option value="default" disabled={true}>Seleccione una opción</option>
                                            <option value="si">Si, cuento con una licencia de condicir</option>
                                            <option value="no">No, Aún no cuento con una licencia de condicir</option>
                                        </select>
                                    </div>
                                    <div className="sub-row">
                                        <span>Ingresa tu correo electrónico</span>
                                        <input
                                            className='input-text'
                                            placeholder='youremail@email.com'
                                            ref={e => this.email = e}
                                            onChange={
                                                (email) => {
                                                    this.setState({
                                                        email: email.target.value,
                                                        validateEmail: util.validateEmail(email.target.value)
                                                    })
                                                }
                                            }
                                            required={true} />
                                    </div>
                                    <div className="sub-row">
                                        <span>Hoja de vida</span>
                                        <input 
                                            className='input-text'
                                            ref={
                                                e => this.pdf = e
                                            } 
                                            type='file' 
                                            accept='application/pdf' 
                                            onChange={
                                                (e) => {
                                                    this.setState({
                                                        pdf: e.target.files
                                                    })
                                                }
                                            } />
                                    </div>                                    
                                </form>
                            </div>

                            <div className="content-button">
                                <button onClick={this.validate}>Aplicar</button>
                            </div>
                        </div>
                        <div className="row info">
                            <h3>Requisitos</h3>
                            <p>
                                El principal requisito para formar parte de la familia de <b>RP SIRZA CONCEPTOS, 
                                LLC</b> es ser una persona con ansias de emprender y cambiar tu vida.
                            </p>
                            <p>
                                <b>Un deseo ardiente;</b> de alcanzar el éxito y darle un giro a tu vida, tomando las oportunidades a tu alcance y 
                                luchando por hacer tus sueños realidad.
                            </p>

                            <p>
                                <b>Hablar español;</b> tu idioma nunca será impedimento para el éxito. Miles de personas han viajado a EEUU sin siquiera saber leer y 
                                eso no les ha sido obstáculo para vivir a plenitud el sueño americano.
                            </p>

                            <p>
                                <b>Ser constante;</b> la perseverancia es una de las cualidades más importantes que todo emprendedor debe tener. Ser constante en tus convicciones 
                                te garantizará el éxito a largo plazo. De grano a grano, se hizo una montaña.
                            </p>

                            <p>
                                <b>Entregarse totalmente;</b> a lo que te apasiona, a tus metas, a lo que amas y a lo que hará que tu vida nunca vuelva a ser la misma. 
                            </p>

                            <p>
                                Si desean saber mas información de lo que se dedica <b>RP, SIRZA CONCEPTOS, LLC</b> haga <a href="./static/pdf/info.pdf" target='_blank'>Click aquí</a>.
                            </p>
                            {/* <img src="./static/img/mapa.PNG" alt="#" /> */}
                        </div>
                    </div>

                    <Loader active={this.state.loader} />
                </div>
                <Footer />
            </div>
        )
    }
}


// NOmbre
// profesion
// nacinalida
// telefono
// curricum

export default Aplicar