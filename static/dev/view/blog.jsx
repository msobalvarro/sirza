import React, { PureComponent } from 'react'
import Axios from 'axios'
import Menu from '../components/menu'
import Footer from '../components/footer'
import Loader from '../components/loader'
import ScrollRevea from 'scrollreveal'
import '../styles/blog'

const Card = (props) => {
    return (
        <div className="card-item">
            <img src="https://via.placeholder.com/600/771796" alt="post" />
            <div className="footer">
                <span className="title">{props.title}</span>
                <p>
                    {props.body}
                </p>
            </div>
        </div>
    )
}

class Blog extends PureComponent {
    constructor(props) {
        super(props)

        this.state = {
            data: [],
            loader: true,
            finalPetition: false,
        }
    }

    async componentDidMount() {
        try {
            const self = this
            await Axios.get('https://jsonplaceholder.typicode.com/posts').then(
                (e) => {
                    self.setState({
                        data: e.data.slice( (e.data.length / 2), e.data.length )
                    })

                    ScrollRevea().reveal('.card-item')
                }
            ).catch(
                (error) => {
                    throw error
                }
            )
        } catch (error) {
            console.log(error)
        } finally {
            this.setState({
                loader: false,
                finalPetition: true,
            })
        }
    }

    render() {
        return (
            <div className="container blog">
                <Menu />

                <div className="content-blog">
                    <Loader active={this.state.loader} />

                    {
                        (this.state.finalPetition && this.state.data.length == 0) &&
                        <h2 className='no-blog'>Aún no se han publicado algún blog</h2>
                    }

                    {
                        this.state.data.map(
                            (item, index) => {
                                return <Card {...item} key={index} />
                            }
                        )
                    }
                </div>

                <Footer />
            </div>
        )
    }
}

export default Blog