import React, { PureComponent } from 'react'
import Menu from '../components/menu'
import Footer from '../components/footer'
import ScrollRevea from 'scrollreveal'
import '../styles/inicio'

const data = new Array(5).fill('e')

class Index extends PureComponent {

    componentDidMount() {
        ScrollRevea().reveal('.fadeIn')
        console.log(data)
    }

    render() {
        return (
            <div className="container index">
                <Menu slider={true} />
                
                <div className="content-inicio">
                
                    <h1 className="title-main">Bienvenido a RP SIRZA CONCEPTOS LLC</h1>
                    <h2 className='sub-title'>Donde tus sueños se pueden hacer realidad.</h2>

                    <img src="./static/img/logo.png" alt="logo" className='logo-main' />

                    <div className="bloque fadeIn">
                        <div className="row">
                            <div className="title">
                                <h2><b>RP SIRZA CONCEPTOS LLC</b></h2>
                                <div className="line"></div>
                            </div>

                            <p>
                                Somos una compañía especializada en creación, formación y desarrollo en negocios y franquicias de ventas directas en los Estados Unidos.
                            </p>
                        </div>
                        <div className="row right">
                            <img src="./static/img/personal.svg" alt=""/>
                        </div>
                    </div>

                    <div className="sub-baner fadeIn">
                        <div className="content-baner">
                            <span className="title-baner">Esclarecemos tus dudas</span>
                            <p className="sub-title-baner">
                                Es importante dejar claro que no somos un negocio piramidal. Nuestro modelo de negocio se basa en las 
                                ventas directas y no requiere una inversión inicial o membresía por parte de nuestros colaboradores ni experiencia previa en el rubro. 
                                Somos una empresa seria y responsable que ha sabido hacerse un lugar en el competitivo mercado actual y estaremos 
                                encantados de esclarecer todas tus dudas y darte la bienvenida a nuestra familia.
                            </p>
                        </div>
                    </div>


                    <div className="bloque fadeIn">
                        <div className="row left">
                            <img src="./static/img/work.svg" alt=""/>
                        </div>
                        <div className="row">
                            <div className="title">
                                <h2>Ruta Rápida al Éxito</h2>
                                <div className="line"></div>
                            </div>

                            <p>
                                Tu aprenderás de una forma tan rápida como sencilla los secretos, estrategias y técnicas que te 
                                ayudarán a potenciar en poco tiempo tu talento natural, logrando una proyección que te permitirá 
                                desarrollar herramientas para consolidarte profesionalmente.
                            </p>
                            <p>
                                Las oportunidades sólo se presentan una vez en la vida y están ahí para aprovecharlas o 
                                dejar que otros lo hagan. Quizá tu sepas qué se siente tocar puertas que no se abren, 
                                trabajar bajo un horario inflexible, buscar un ingreso adicional y no encontrar la actividad 
                                que te permita obtenerlo, querer emprender o entrar en un negocio y no tener ni la instrucción, 
                                ni el apoyo, ni el dinero para hacerlo. No estás solo, puedes confiar plenamente en nosotros.
                            </p>
                        </div>
                    </div>

                </div>

                <Footer />

            </div>
        )
    }
}
export default Index