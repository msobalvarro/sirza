import React, { PureComponent } from 'react'
import { Link } from  'react-router-dom'
import Menu from '../components/menu'
import Footer from '../components/footer'
import '../styles/comunidad'

class Nosotros extends PureComponent {
    constructor(props) {
        super(props)

        this.state = {
            array: new Array(10).fill(true),
            modal: false,
        }
    }

    showModal = (url = '') => {
        // console.log(url)
        
        try {
            this.setState({
                modal: true
            }, () => {
                this.imagen.src = url
            })
        } catch (error) {
            // console.log(error)
            console.log(this.imagen)
        }
    }

    render () {
        return (
            <div className="container nosotros">
                <Menu />

                <div className="content-nosotros">

                    <h2>Nuestra Comunidad</h2>

                    <p>
                        En RP SIRZA CONCEPTOS LLC nos enorgullece decir que somos gestores de nuevas oportunidades para personas 
                        que quieren darle un nuevo horizonte a sus vidas y consolidarse como profesionales.  Somos cada vez más grandes 
                        y estamos felices de que nuestra familia crezca cada día más y a su vez brindando una oportunidad de cambio 
                        a tantas personas que en la actualidad forman parte activa de <b>RP SIRZA CONCEPTOS LLC</b>
                    </p>
                    <div className="grid">
                        {
                            this.state.array.map(
                                (data, index) => {
                                    const url = `./static/img/galeria/img${(index + 1)}.jpeg`

                                    console.log(index)
                                    
                                    return (
                                        <div 
                                            key={index}
                                            className='item' 
                                            style={{ 
                                                backgroundImage: `url(${url})`,
                                            }} 
                                            onClick={
                                                (e) => {
                                                    this.showModal(url)
                                                }
                                            } />
                                    )
                                }
                            )
                        }
                    </div>

                    {
                        (this.state.modal) &&
                        <div className="modal">
                            <div className="close-modal" onClick={
                                () => {
                                    this.setState({
                                        modal: false
                                    })
                                }
                            }></div>
                            <img src="" alt="" ref={
                                e => this.imagen = e
                            } />
                        </div>
                    }

                </div>
                <Footer />
            </div>
        )
    }
}

export default Nosotros