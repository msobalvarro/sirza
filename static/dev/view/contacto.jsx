import React, { PureComponent } from 'react'
import Axios from 'axios'
import swal from 'sweetalert'
import Menu from '../components/menu'
import '../styles/contacto'
import Footer from '../components/footer'
import MediaQuery from 'react-responsive'
import util from '../util'
import Loader from '../components/loader';
// import Captcha from 'react-recaptcha'
// import Footer from '../components/footer'


class Contacto extends PureComponent {
    constructor(props) {
        super(props)

        this.validate = this.validate.bind(this)

        this.state = {
            nombre: '',
            apellido: '',
            telefono: '',
            email: '',
            mensaje: '',
            active: false,
            loader: false
        }
    }

    async validate(e) {
        this.setState({
            loader: true
        })
        // Validación de formulario
        e.preventDefault()
        // console.log(grecaptcha.getResponse())

        // declaración de variable
        // Esta variable definirá si la petición hacia el servidor se ejecutará
        let send = true

        // Comienzo con las validaciones de los campos y validación de reCaptcha
        try {
            // if (grecaptcha.getResponse().length == 0)
            //     throw 'Validación de Captcha incorrecta'

            if (this.state.nombre.length == 0) {
                throw 'Ingrese al menos un nombre'

                this.nombre.focus()
            }

            if (this.state.apellido.length == 0) {
                throw 'Ingrese al menos un apellido'
                this.apellido.focus()
            }

            if (this.state.telefono.length > 8) {
                throw 'Ingrese un número de teléfono válido'
                this.telefono.focus()
            }

            if (!util.validateEmail(this.state.email)) {
                throw 'Ingrese una direccioón de correo válido'
                this.email.focus()
            }

        } catch (error) {
            send = false

            swal({
                icon: 'warning',
                title: 'Rellene todos los campos',
                text: error
            })
        } finally {
            if (send) {
                const self = this
                const model = {
                    nombre: this.state.nombre,
                    apellido: this.state.apellido,
                    telefono: this.state.telefono,
                    email: this.state.email,
                    mensaje: this.state.mensaje,
                    tipo: 'info',
                    // captcha: grecaptcha.getResponse(),
                }

                await Axios.post(`sendMail/index.php`, model).then(
                    // Axios.post(`${util.url}/sendMail`, model).then(
                    (response) => {
                        console.log(response.data)

                        if (response.data.success) {
                            swal({
                                icon: 'success',
                                title: 'Gracias por contactarnos',
                                text: 'Estaremos respondiendo a su correo lo mas antes posible'
                            })

                            this.nombre.value = ''
                            this.apellido.value = ''
                            this.telefono.value = ''
                            this.email.value = ''
                            this.mensaje.value = ''

                            self.setState({
                                nombre: '',
                                apellido: '',
                                telefono: '',
                                email: '',
                                mensaje: ''
                            })
                        }

                        if (response.data.error) {
                            swal({
                                icon: 'error',
                                title: response.data.message
                            })
                        }

                    }
                )
            }

            self.setState({
                loader: false
            })
        }
    }

    render() {
        return (
            <div className="container contacto">
                <Menu />

                <div className="content-contacto">
                    <div className={`map ${!this.state.active ? 'blur' : ''} `}>
                        <iframe
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2527.6251828943045!2d-100.92448194371573!3d37.04298884286823!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8708138f3362a08b%3A0x7eb0571348b60f63!2s504+N+Kansas+Ave%2C+Liberal%2C+KS+67901%2C+EE.+UU.!5e0!3m2!1ses!2sni!4v1547184284033"
                            frameBorder="0"
                            style={{ border: 0 }}
                            allowFullScreen></iframe>
                        <div className={`content-map ${this.state.active ? 'fade' : null}`}>
                            <span className="title">
                                Contacto
                            </span>
                            <span className="subtitle">
                                No dudes en contactarnos. Nuestro equipo de trabajo establecerá tus dudas.
                            </span>
                        </div>
                    </div>

                    <div className={`formulario ${this.state.active ? 'active' : ''}`}>
                        <MediaQuery minDeviceWidth={1224}>
                            <div className="content-float">
                                <div className={`float-button ${(this.state.active) ? 'active' : null}`} onClick={
                                    () => {
                                        this.setState({
                                            active: !this.state.active
                                        })
                                    }
                                }>
                                    <img src={`./static/img/${(this.state.active) ? 'close' : 'location'}.svg`} alt="" />
                                </div>
                            </div>
                        </MediaQuery>
                        <div className="sub-content">
                            <div className="row">
                                <form action="#" onSubmit={this.validate}>
                                    <div className="col">
                                        <input
                                            className='input-text'
                                            placeholder='Nombre'
                                            onChange={nombre => this.setState({ nombre: nombre.target.value })}
                                            ref={e => this.nombre = e}
                                        />

                                        <input
                                            className='input-text'
                                            placeholder='Apellido'
                                            onChange={apellido => this.setState({ apellido: apellido.target.value })}
                                            ref={e => this.apellido = e}
                                        />
                                    </div>

                                    <div className="col">
                                        <input
                                            className='input-text'
                                            placeholder='Correo Electrónico'
                                            onChange={email => this.setState({ email: email.target.value })}
                                            ref={e => this.email = e}
                                        />

                                        <input
                                            className='input-text'
                                            placeholder='Telefono'
                                            onChange={telefono => this.setState({ telefono: telefono.target.value })}
                                            ref={e => this.telefono = e}
                                        />
                                    </div>


                                    <div className="col full">
                                        <textarea placeholder='Mensaje'
                                            onChange={
                                                (e) => {
                                                    this.setState({
                                                        mensaje: e.target.value
                                                    })
                                                }
                                            }
                                            ref={e => this.mensaje = e}></textarea>
                                    </div>

                                    {/* <Captcha
                                        ref={e => this.captcha = e}
                                        sitekey='6LehfYEUAAAAALs77mp4kDOGGvGUtpNHP24Ox41p'
                                        hl='es'
                                        className='g-recaptcha' /> */}

                                    {/* <div className="g-recaptcha" data-sitekey="6LfDPnsUAAAAAEhfpihGcSpzO3zqJI5r9rpvJ9_R" ref={e => this.capchat = e}></div> */}

                                    <div className="col full">
                                        <button type='submit'>Enviar</button>
                                    </div>
                                </form>
                            </div>
                            <div className="row">
                                <div className="col block">
                                    <span className='header'>Llámanos</span>
                                    <span>Sirza Altamirano: <a href="tel:+1_(620) 453-0437">+1_(620) 453-0437</a></span>
                                    <span>Oscar J. Martínez: <a href="tel:+1_(316) 648-0883">+1_(316) 648-0883</a></span>
                                </div>

                                <div className="col block">
                                    <span className='header'>Escríbenos</span>
                                    <span>Info@conceptosrpsirza.com</span>
                                </div>

                                <div className="col block">
                                    <span className='header'>Visítanos</span>
                                    <span>
                                        Dirección de oficina:  504 N Kansas Avenue (A4) Liberal, KS 67901
                                    </span>
                                </div>

                                <div className="col social">
                                    <a href="https://www.facebook.com/ConceptosRPSirza" className="facebook" target='_blank'></a>
                                    <a href="https://www.instagram.com/sirza_conceptos/" className="instagram" target='_blank'></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <Loader active={this.state.loader} />
                </div>

                {/* <Footer /> */}
                <MediaQuery maxDeviceWidth={1224}>
                    <Footer />
                </MediaQuery>
            </div>
        )
    }
}

export default Contacto