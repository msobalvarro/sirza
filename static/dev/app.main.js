import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { HashRouter, Route, Switch } from 'react-router-dom'
import './styles/main'
import Index from './view/index'
import Aplicar from './view/aplicar'
// import Blog from './view/blog'
import Nosotros from './view/comunidad'
import Historia from './view/historia'
import Contacto from './view/contacto'

class Main extends Component {
	render() {
		return (
			<HashRouter>
                <Switch>
                    <Route exact path="/" component={Index} />
                    <Route exact path="/aplicar" component={Aplicar} />
                    {/* <Route exact path="/blog" component={Blog} /> */}
                    <Route exact path="/comunidad" component={Nosotros} />
                    <Route exact path="/historia" component={Historia} />
                    <Route exact path="/contacto" component={Contacto} />
                </Switch>
            </HashRouter>
		)
	}
}

ReactDOM.render(<Main />, document.getElementById('main')) 