<?php

// Configuramos el archivo para que pueda recibir parametros por medio 
// de peticiones POST
$postdata = file_get_contents("php://input", true);
$request = json_decode($postdata);

// $captcha = $request->captcha;


// recaptcha.php es un archivo para procesar las captchas al usarse
// require_once 'recaptcha.php';

// clave secreta de captcha
// Clave generada en https://www.google.com/recaptcha con cuenta de mssrsobalvarro1997@gmail.com
// $secret = "6LehfYEUAAAAAFNMn3GDP_0Es74tburNW-ap_ONk";
 
// Verificamos la clave secreta
// $reCaptcha = new ReCaptcha($secret);

// Verificamos si recibe un captcha (id de captcha)
// if($captcha)
// {
    

// }
// else
// {
//     $data = array('error' => true, 'message' => 'dato no encontrado');
//     echo json_encode($data);
// }

try {
    $tipo = $request->tipo;
    // $ip = $_SERVER['REMOTE_ADDR'];
    // $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$captcha."&remoteip=".$ip);
    // $responseKeys = json_decode($response, true);

    // $data = array('nombre' => $nombre, 'apellido' => $apellido, 'email' => $email, 'mensaje' => $mensaje);
    // echo json_encode($data);     

    // Validamos que tipo de correo es info/apply
    if($tipo == 'info') {
        $sendTo = 'info@conceptosrpsirza.com';
        $asunto = "Información";

        // Creamos las variables capturando los datos que envía ajax
        $nombre = $request->nombre;
        $apellido = $request->apellido;
        $email = $request->email;
        $mensaje = $request->mensaje;
        $telefono = $request->telefono;       

        // Armamos un mensaje básico
        $mensajeFinal = "De ".$nombre." ".$apellido." - ".$email." \n \n ".$mensaje;

    } 
    elseif($_POST['tipo'] == 'apply') {
        // Configuramos el destino y el asunto del correo
        $sendTo = 'apply@conceptosrpsirza.com';
        $asunto = "Aplicar";

        // Verificamos si existe el archivo de correo y si es tipo PDF
        if($_FILES['cv']['name'] != null && $_FILES['cv']['type'] == "application/pdf") {

            // Verifica si se copió el archivo a la ruta prevista `../documentos/`
            if(copy($_FILES['cv']['tmp_name'], '../documentos/'.$_POST['nombre'].".pdf" )) {
                // Si se ha ejecutado la copia del archivo configurar el mensaje de envío
                $mensajeFinal = "De ".$_POST['nombre']." - ".$_POST['email'].
                " \n \n \nNacionalidad: ".$_POST['nacionalidad']."\n Profesión: ".$_POST['profesion'].
                "\n licencia de conducir: ".$_POST['licencia']." \n \n CV: http://conceptosrpsirza.com/documentos/".$_POST['nombre'].".pdf";                
            } else {
                throw new Exception("No se ha podido copiar el archivo");
            }

        } else {
            throw new Exception("No se pudo encontrar el pdf", 1);
        }

    } else {
        throw new Exception("No se encontró tipo de servicio", 1);
    }

    // echo json_encode($_FILES['cv']['name']);
    
    // Configuramos los headers para el envío de correo
    $header = 'From: ' . $email . " \r\n";
    $header .= "Content-Type: text/plain; charset=UTF-8";

    if(mail($sendTo, $asunto, $mensajeFinal, $header)) {
        $data = array('success' => true);

        echo json_encode($data);
    } else {
        throw new Exception("El correo no se pudo enviar", 1);
    }



    // $datos = array('captcha' => $captcha, 'ip' => $ip, 'response' => $responseKeys);
    // if(intval($responseKeys["success"]) !== 1) {
    //   echo 'ReCaptcha incorrecto';
    // } else {
    //     if(mail($destino, $asunto, $mensaje))
    //     {
    //         echo "Correo Enviado";
    //     } 
    //     else {
    //         echo "Correo NO Enviado";    
    //     }

    //     echo "Validación correcta";
    // }

    // echo json_encode($datos);
} 
catch (Exception $e) {
    // Armamos un json con la siguiente estructura
    // {
    //     error: true,
    //     message: 'error de exeption'
    // }
    $data_error = array('error' => true, 'message' => $e);
    echo json_decode($data_error);
}
